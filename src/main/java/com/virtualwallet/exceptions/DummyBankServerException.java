package com.virtualwallet.exceptions;

public class DummyBankServerException extends RuntimeException {

    private static final String DUMMY_BANK_SERVER_ERROR_MESSAGE = "DummyBank server is down.";

    public DummyBankServerException() {
        super(DUMMY_BANK_SERVER_ERROR_MESSAGE);
    }
}
