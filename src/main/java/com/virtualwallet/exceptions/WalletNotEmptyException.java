package com.virtualwallet.exceptions;

public class WalletNotEmptyException extends RuntimeException{

    private static final String WALLET_NOT_EMPTY_ERROR_MESSAGE = "Please withdraw all your money, before deleting your account.";

    public WalletNotEmptyException() {
        super(WALLET_NOT_EMPTY_ERROR_MESSAGE);
    }
}
