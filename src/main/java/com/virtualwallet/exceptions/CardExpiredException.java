package com.virtualwallet.exceptions;

public class CardExpiredException extends RuntimeException{

    private static final String CARD_EXPIRED_ERROR_MESSAGE = "Your card has expired!";

    public CardExpiredException() {
        super(CARD_EXPIRED_ERROR_MESSAGE);
    }
}
