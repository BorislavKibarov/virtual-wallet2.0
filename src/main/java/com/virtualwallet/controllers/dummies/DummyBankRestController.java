package com.virtualwallet.controllers.dummies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api/banks")
public class DummyBankRestController {

    private final Random random;

    @Autowired
    public DummyBankRestController(Random random) {
        this.random = random;
    }

    @GetMapping
    public HttpStatus isApproved() {
        boolean isApproved = random.nextBoolean();

            if (isApproved) {
                return HttpStatus.OK;
            } else {
                return HttpStatus.BAD_REQUEST;
            }
    }
}
