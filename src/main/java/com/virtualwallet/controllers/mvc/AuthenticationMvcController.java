package com.virtualwallet.controllers.mvc;


import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.AuthenticationFailureException;
import com.virtualwallet.exceptions.DuplicateEntityException;
import com.virtualwallet.mappers.UserMapper;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.LoginDto;
import com.virtualwallet.models.dtos.RegisterDto;
import com.virtualwallet.models.dtos.UserDto;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    private static final String STANDARD_LOGIN_HEADER = "Welcome back!";
    private static final String EMAIL_CONFIRMATION_LOGIN_HEADER = "Welcome back! You've successfully verified your email.";
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationMvcController(UserService userService,
                                       AuthenticationHelper authenticationHelper,
                                       UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        model.addAttribute("header", STANDARD_LOGIN_HEADER);
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());

            session.setAttribute("currentUser", user.getUsername());
            session.setAttribute("userId", user.getId());

            return "redirect:/panel";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto registerDto,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "register";
        }

        try {
            UserDto userDto = userMapper.fromRegisterDtoToUserDto(registerDto);
            User user = userMapper.fromUserDtoToUserObject(userDto);
            userService.create(user);

            return "redirect:/auth/email-verification";

        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "register";
        }
    }

    @GetMapping("/email-verification")
    public String showEmailVerification() {
        return "email-confirmation";
    }

    @GetMapping("/email-confirmation")
    public String handleEmailVerification(@RequestParam("token") String token, Model model) {
        try {
            userService.confirmUser(token);
            model.addAttribute("login", new LoginDto());
            model.addAttribute("header", EMAIL_CONFIRMATION_LOGIN_HEADER);
            return "login";
        } catch (IllegalArgumentException e) {
            return "redirect:/panel";
        }
    }
}
