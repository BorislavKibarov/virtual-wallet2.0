package com.virtualwallet.controllers.mvc;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.*;
import com.virtualwallet.mappers.CardMapper;
import com.virtualwallet.mappers.TransactionMapper;
import com.virtualwallet.models.Card;
import com.virtualwallet.models.Status;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.*;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.NotificationService;
import com.virtualwallet.services.interfaces.TransactionService;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

import static com.virtualwallet.services.TransactionServiceImpl.BLOCKED_USER_ERROR_MESSAGE;
import static com.virtualwallet.services.TransactionServiceImpl.EMAIL_NOT_CONFIRMED_ERROR_MESSAGE;

@Controller
@RequestMapping("/transactions")
@SessionAttributes({"transaction", "senderUsername", "recipientUsername", "view"})
public class TransactionMvcController {

    private final TransactionService transactionService;
    private final AuthenticationHelper authenticationHelper;
    private final TransactionMapper transactionMapper;
    private final CardService cardService;
    private final CardMapper cardMapper;
    private final NotificationService notificationService;
    private final UserService userService;

    @Autowired
    public TransactionMvcController(TransactionService transactionService, AuthenticationHelper authenticationHelper, TransactionMapper transactionMapper, CardService cardService, CardMapper cardMapper, NotificationService notificationService, UserService userService) {
        this.transactionService = transactionService;
        this.authenticationHelper = authenticationHelper;
        this.transactionMapper = transactionMapper;
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.notificationService = notificationService;
        this.userService = userService;
    }

    @ModelAttribute("userId")
    public Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("userId");
    }

    @ModelAttribute("notifications")
    public Long populateNotificationsNumber(HttpSession session) {
        return notificationService.getUnread((Integer) session.getAttribute("userId"));
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getDisplayValue().equals("Admin"));
    }

    @GetMapping
    public String showAllTransactions(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
            List<Transaction> list = transactionService.getAll();
            Collections.reverse(list);
            model.addAttribute("transactions", list);
            model.addAttribute("filterDto", new FilterTransactionDto());

            return "transactions";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/user")
    public String showAllUserTransactions(@PathVariable int id,
                                          @ModelAttribute FilterTransactionDto transactionDto,
                                          Model model,
                                          HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);

            List<Transaction> list = transactionService.getAllForUser(
                    user,
                    id,
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty(),
                    Optional.empty());

            populateSendersAndRecipients(list, model);

            List<ViewTransactionDto> result = transactionMapper.convertList(transactionService.getAllForUser(
                    user,
                    id,
                    Optional.ofNullable(transactionDto.getFromDate()),
                    Optional.ofNullable(transactionDto.getToDate()),
                    Optional.ofNullable(transactionDto.getSenderId()),
                    Optional.ofNullable(transactionDto.getRecipientId()),
                    Optional.empty(),
                    Optional.empty()), id);


            model.addAttribute("transactions", result);
            model.addAttribute("filterDto", new FilterTransactionDto());

            return "user-transactions";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new/transfer")
    public String showTransferPage(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (user.getStatus().equals(Status.BLOCKED)) {
                model.addAttribute("error", BLOCKED_USER_ERROR_MESSAGE);
                return "blocked";
            }
            if (!user.isEmailConfirmed()) {
                model.addAttribute("error", EMAIL_NOT_CONFIRMED_ERROR_MESSAGE);
                return "blocked";
            }
            model.addAttribute("transaction", new TransferTransactionDto());
            model.addAttribute("senderUsername", user.getUsername());

            return "transaction-new-transfer";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new/{id}/contact-transfer")
    public String showContactTransferPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (user.getStatus().equals(Status.BLOCKED)) {
                model.addAttribute("error", BLOCKED_USER_ERROR_MESSAGE);
                return "blocked";
            }
            if (!user.isEmailConfirmed()) {
                model.addAttribute("error", EMAIL_NOT_CONFIRMED_ERROR_MESSAGE);
                return "blocked";
            }
            model.addAttribute("transaction", new TransferTransactionDto(userService.getById(id).getUsername()));
            model.addAttribute("senderUsername", user.getUsername());

            return "transaction-new-contact-transfer";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/transfer/confirm")
    public String confirmTransfer(@ModelAttribute("transaction") TransferTransactionDto transactionDto,
                                  @ModelAttribute("senderUsername") String username,
                                  Model model) {
        transactionDto.setSender(username);
        model.addAttribute("transaction", transactionDto);
        model.addAttribute("view", "transaction-new-transfer");

        return "transfer-confirmation";
    }

    @GetMapping("/new/deposit")
    public String showDepositPage(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("transaction", new DepositTransactionDto());
            model.addAttribute("recipientUsername", user.getUsername());
            model.addAttribute("cards", cardMapper.convertList(cardService.getAllForUser(user.getId())));

            return "transaction-new-deposit";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/deposit/confirm")
    public String confirmDeposit(@ModelAttribute("transaction") DepositTransactionDto transactionDto,
                                 @ModelAttribute("recipientUsername") String username,
                                 Model model) {
        try {
            transactionDto.setSender("DummyBank");
            transactionDto.setRecipient(username);
            Card card = cardService.getValidCard(transactionDto.getCardId());
            model.addAttribute("transaction", transactionDto);
            model.addAttribute("card", new ViewCardDto(card));
            model.addAttribute("view", "transaction-new-deposit");

            return "deposit-confirmation";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/transactions/new/deposit";
        } catch (CardExpiredException e) {
            model.addAttribute("error", e.getMessage());
            return "card-expired";
        }
    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute("transaction") TransferTransactionDto transactionDto,
                                    BindingResult errors,
                                    @ModelAttribute("view") String view,
                                    Model model) {
        if (errors.hasErrors()) {
            return view;
        }
        try {
            Transaction transaction = transactionMapper.fromTransactionDtoToTransactionObject(transactionDto);
            transactionService.create(transaction);

            return "redirect:/panel";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (InsufficientFundsException e) {
            model.addAttribute("error", e.getMessage());
            return "bank-error";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return view;
        }
    }

    private void populateSendersAndRecipients(List<Transaction> list, Model model) {
        Set<User> senders = new HashSet<>();
        Set<User> recipients = new HashSet<>();

        for (Transaction transaction : list) {
            senders.add(transaction.getSender());
            recipients.add(transaction.getRecipient());
        }

        model.addAttribute("senders", senders);
        model.addAttribute("recipients", recipients);
    }
}