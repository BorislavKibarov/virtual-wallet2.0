package com.virtualwallet.controllers.mvc;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.*;
import com.virtualwallet.mappers.TransactionMapper;
import com.virtualwallet.mappers.UserMapper;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.FilterTransactionDto;
import com.virtualwallet.models.dtos.PasswordDto;
import com.virtualwallet.models.dtos.UpdateUserDto;
import com.virtualwallet.models.dtos.ViewTransactionDto;
import com.virtualwallet.services.interfaces.NotificationService;
import com.virtualwallet.services.interfaces.TransactionService;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final NotificationService notificationService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;

    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, NotificationService notificationService, TransactionService transactionService, TransactionMapper transactionMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.notificationService = notificationService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }

    @ModelAttribute("userId")
    public Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("userId");
    }

    @ModelAttribute("notifications")
    public Long populateNotificationsNumber(HttpSession session) {
        return notificationService.getUnread((Integer) session.getAttribute("userId"));
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getDisplayValue().equals("Admin"));
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
            model.addAttribute("users", userService.search(Optional.empty()));

            return "users";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
            model.addAttribute("user", userService.getById(id));

            return "user";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }


    @PostMapping("/upload")
    public String upload(MultipartFile file, HttpSession session, Model model) throws IOException {
        try {
            User user = authenticationHelper.tryGetUser(session);
            userService.upload(file, user);
        } catch (FileNotFoundException e) {
            return "redirect:/users/profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        return "redirect:/users/profile";
    }

    @GetMapping("/profile")
    public String showProfile(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", userService.getById(user.getId()));

            return "profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/update")
    public String showUpdatePage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            UpdateUserDto userDto = userMapper.fromUserToUpdateUserDto(user);
            model.addAttribute("userDto", userDto);

            return "user-update";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/update")
    public String updateUser(@Valid @ModelAttribute("userDto") UpdateUserDto userDto,
                             BindingResult errors,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "user-update";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            user = userMapper.fromUpdateUserDtoToUserObject(userDto, user);

            userService.update(user);
            return "redirect:/users/profile";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_user", e.getMessage());
            return "user-update";
        }
    }

    @GetMapping("/password")
    public String showChangePasswordPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("passwordDto", new PasswordDto());

            return "user-change-password";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/password")
    public String changePassword(@Valid @ModelAttribute("passwordDto") PasswordDto passwordDto,
                                 BindingResult errors,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "user-change-password";
        }
        if (!passwordDto.getNewPassword().equals(passwordDto.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "password_error", "Password confirmation should match password.");
            return "user-change-password";
        }

        User user = authenticationHelper.tryGetUser(session);

        if (!passwordDto.getUserPassword().equals(user.getPassword())) {
            errors.rejectValue("userPassword", "password_error", "Wrong password");
            return "user-change-password";
        }

        user = userMapper.changeUserPassword(passwordDto, user);

        userService.update(user);
        return "redirect:/users/profile";
    }

    @GetMapping("/delete")
    public String deleteUser(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            userService.delete(user);

            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (WalletNotEmptyException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("user", authenticationHelper.tryGetUser(session));
            return "profile";
        }
    }

    @PostMapping("{id}/role")
    public String changeRole(@PathVariable int id, HttpSession session) {
        authenticationHelper.tryGetAdmin(session);
        User user = userMapper.changeUserRole(id);
        userService.update(user);

        return String.format("redirect:/users/%s", id);
    }

    @PostMapping("{id}/status")
    public String changeStatus(@PathVariable int id, HttpSession session) {
        authenticationHelper.tryGetAdmin(session);
        User user = userMapper.changeUserStatus(id);
        userService.update(user);

        return String.format("redirect:/users/%s", id);
    }

    @GetMapping("/{id}/contacts")
    public String showContacts(@PathVariable int id,
                               Model model,
                               HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            List<User> recipients = userService.getRecipients(id);
            model.addAttribute("recipients", recipients);

            return "contacts";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }
}
