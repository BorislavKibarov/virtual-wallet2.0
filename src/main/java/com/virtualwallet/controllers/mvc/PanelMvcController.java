package com.virtualwallet.controllers.mvc;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.AuthenticationFailureException;
import com.virtualwallet.mappers.TransactionMapper;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.Wallet;
import com.virtualwallet.models.dtos.ViewTransactionDto;
import com.virtualwallet.services.interfaces.NotificationService;
import com.virtualwallet.services.interfaces.TransactionService;
import com.virtualwallet.services.interfaces.WalletService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class PanelMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final WalletService walletService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final NotificationService notificationService;


    public PanelMvcController(AuthenticationHelper authenticationHelper, WalletService walletService, TransactionService transactionService, TransactionMapper transactionMapper, NotificationService notificationService) {
        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.notificationService = notificationService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getDisplayValue().equals("Admin"));
    }

    @ModelAttribute("userId")
    public Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("userId");
    }

    @ModelAttribute("wallet")
    public Wallet populateUserWallet(HttpSession session) {
        return walletService.getByUserId((Integer) session.getAttribute("userId"));
    }

    @ModelAttribute("transactions")
    public List<ViewTransactionDto> populateUserTransactions(HttpSession session) {
        List<Transaction> list = transactionService.getLastThree((Integer) session.getAttribute("userId"));

        return transactionMapper.convertList(list, (Integer) session.getAttribute("userId"));
    }

    @ModelAttribute("notifications")
    public Long populateNotificationsNumber(HttpSession session) {
        return notificationService.getUnread((Integer) session.getAttribute("userId"));
    }

    @GetMapping
    public String showHomePage(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            return "panel";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }
}
