package com.virtualwallet.controllers.rest;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.DuplicateEntityException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.mappers.UserMapper;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.UpdateUserDto;
import com.virtualwallet.models.dtos.UserDto;
import com.virtualwallet.services.interfaces.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private static final String STATUS_INVALID = "%s is not a valid status.";
    private static final String ROLE_INVALID = "%s is not a valid role.";
    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService service, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all users.")
    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a user based on id.")
    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all users matching a given value for username, email and/or phone number.")
    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> keyWord) {
        authenticationHelper.tryGetUser(headers);

        return service.search(keyWord);
    }

    @GetMapping("/{id}/contacts")
    public List<User> getContacts(@PathVariable int id) {

        return service.getRecipients(id);
    }

    @ApiOperation(value = "Creates a user.")
    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromUserDtoToUserObject(userDto);
            service.create(user);

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Blocks/unblocks a user.")
    @PutMapping("/status/{id}")
    public User changeStatus(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            User user = userMapper.changeUserStatus(id);
            service.update(user);

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Updates user's role.")
    @PutMapping("/role/{id}")
    public User changeRole(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            User user = userMapper.changeUserRole(id);
            service.update(user);

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Updates a user.")
    @PutMapping
    public User update(@RequestHeader HttpHeaders headers, @Valid @RequestBody UpdateUserDto updateUserDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            authenticatedUser = userMapper.fromUpdateUserDtoToUserObject(updateUserDto, authenticatedUser);
            service.update(authenticatedUser);

            return authenticatedUser;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes a user.")
    @DeleteMapping
    public void delete(@RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            service.delete(authenticatedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
