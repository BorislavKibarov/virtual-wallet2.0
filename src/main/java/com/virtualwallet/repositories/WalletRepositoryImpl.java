package com.virtualwallet.repositories;

import com.virtualwallet.models.Wallet;
import com.virtualwallet.repositories.interfaces.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class WalletRepositoryImpl extends BaseModifyRepositoryImpl<Wallet> implements WalletRepository {

    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        super(Wallet.class, sessionFactory);
    }

    @Override
    public void update(Wallet senderWallet, Wallet recipientWallet) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(senderWallet);
            session.update(recipientWallet);
            session.getTransaction().commit();
        }
    }
}
