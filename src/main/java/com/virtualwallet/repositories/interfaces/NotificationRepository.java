package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.Notification;

import java.util.List;

public interface NotificationRepository extends BaseGetRepository<Notification>, CreatableRepository<Notification>, UpdatableRepository<Notification>, DeletableRepository<Notification>{

    List<Notification> getAllForUser(int userId);

    Long getUnread(int userId);

    void deleteAllUserNotifications(int userId);
}
