package com.virtualwallet.repositories.interfaces;

public interface UpdatableRepository<T> {
    void update(T entity);
}
