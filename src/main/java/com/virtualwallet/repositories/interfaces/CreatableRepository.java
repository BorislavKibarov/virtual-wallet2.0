package com.virtualwallet.repositories.interfaces;

public interface CreatableRepository<T> {
    void create(T entity);
}
