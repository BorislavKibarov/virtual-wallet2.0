package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.Transaction;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends BaseGetRepository<Transaction>, CreatableRepository<Transaction> {
    List<Transaction> getAllForUser(int id,
                                    Optional<LocalDate> fromDate,
                                    Optional<LocalDate> toDate,
                                    Optional<Integer> senderId,
                                    Optional<Integer> recipientId,
                                    Optional<String> sortAmount,
                                    Optional<String> sortDate);

    List<Transaction> getLastThree(int userId);
}
