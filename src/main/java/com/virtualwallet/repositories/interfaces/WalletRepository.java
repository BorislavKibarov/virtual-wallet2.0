package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.Wallet;

public interface WalletRepository extends BaseGetRepository<Wallet>, CreatableRepository<Wallet>, UpdatableRepository<Wallet>, DeletableRepository<Wallet> {
    void update(Wallet senderWallet, Wallet recipientWallet);
}
