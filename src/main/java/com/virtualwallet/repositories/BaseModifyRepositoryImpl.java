package com.virtualwallet.repositories;

import com.virtualwallet.repositories.interfaces.CreatableRepository;
import com.virtualwallet.repositories.interfaces.DeletableRepository;
import com.virtualwallet.repositories.interfaces.UpdatableRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseModifyRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements CreatableRepository<T>, UpdatableRepository<T>, DeletableRepository<T> {

    public BaseModifyRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public void create(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.save(entity);
        }
    }

    @Override
    public void update(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        }
    }
}
