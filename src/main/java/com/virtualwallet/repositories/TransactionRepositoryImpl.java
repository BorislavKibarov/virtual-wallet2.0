package com.virtualwallet.repositories;

import com.virtualwallet.models.Transaction;
import com.virtualwallet.repositories.interfaces.TransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class TransactionRepositoryImpl extends BaseModifyRepositoryImpl<Transaction> implements TransactionRepository {

    private final SessionFactory sessionFactory;

    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(Transaction.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Transaction> getAllForUser(int id,
                                           Optional<LocalDate> fromDate,
                                           Optional<LocalDate> toDate,
                                           Optional<Integer> senderId,
                                           Optional<Integer> recipientId,
                                           Optional<String> sortAmount,
                                           Optional<String> sortDate) {
        try (Session session = sessionFactory.openSession()) {
            String baseQuery = "";
            if (sortAmount.isPresent() && (sortAmount.equals(Optional.of("asc")) || sortAmount.equals(Optional.of("desc")))) {
                baseQuery += " order by amount " + sortAmount.get();
            }
            if (sortDate.isPresent() && (sortDate.equals(Optional.of("asc")) || sortDate.equals(Optional.of("desc")))) {
                baseQuery += " order by date " + sortDate.get();
            }
            Query<Transaction> query = session.createQuery(
                    "from Transaction where (:recipientId != 0 or sender.id = :id or recipient.id = :id)" +
                            " and (:recipientId = 0 or :recipientId = :id and recipient.id = :recipientId or recipient.id = :recipientId and sender.id = :id)" +
                            " and (:senderId = 0 or sender.id = :senderId)" +
                            " and (:fromDate = null or date >= :fromDate)" +
                            " and (:toDate = null or date <= :toDate)" + baseQuery, Transaction.class);
            query.setParameter("id", id);
            query.setParameter("fromDate", fromDate.orElse(null));
            query.setParameter("toDate", toDate.orElse(null));
            query.setParameter("senderId", senderId.orElse(0));
            query.setParameter("recipientId", recipientId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Transaction> getLastThree(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where recipient.id = :userId or sender.id = :userId order by date desc", Transaction.class);
            query.setParameter("userId", userId);
            query.setMaxResults(3);

            return query.list();
        }
    }
}
