package com.virtualwallet.services;

import com.virtualwallet.exceptions.CardExpiredException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.Card;
import com.virtualwallet.repositories.interfaces.CardRepository;
import com.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CardServiceImpl extends BaseGetServiceImpl<Card> implements CardService {

    public static final String INVALID_CARD_TO_DELETE_ERROR_MESSAGE = "No such card found";
    private final CardRepository repository;

    @Autowired
    public CardServiceImpl(CardRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<Card> getAllForUser(int userId) {
        return repository.getAllForUser(userId);
    }

    @Override
    public void create(Card card) {
        if (isExpired(card)) {
            throw new CardExpiredException();
        }
        repository.create(card);
    }

    @Override
    public void delete(Card card, int userId) {
        if (card.getUser().getId() != userId) {
            throw new UnauthorizedOperationException(INVALID_CARD_TO_DELETE_ERROR_MESSAGE);
        }
        repository.delete(card);
    }

    @Override
    public Card getValidCard(int cardId) {
        Card card = repository.getById(cardId);
        if (isExpired(card)) {
            throw new CardExpiredException();
        }
        return card;
    }

    @Override
    public void deleteAllForUser(int userId) {
        repository.deleteAllForUser(userId);
    }

    private boolean isExpired(Card card) {
        return card.getExpirationDate().isBefore(LocalDate.now());
    }
}
