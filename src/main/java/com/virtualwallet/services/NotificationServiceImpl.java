package com.virtualwallet.services;

import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.Notification;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.repositories.interfaces.NotificationRepository;
import com.virtualwallet.services.interfaces.EmailSenderService;
import com.virtualwallet.services.interfaces.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl extends BaseGetServiceImpl<Notification> implements NotificationService {

    private static final String INVALID_ROLE_ERROR_MESSAGE = "You can only %s your own notifications.";

    private final NotificationRepository notificationRepository;
    private final EmailSenderService emailSenderService;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository, EmailSenderService emailSenderService) {
        super(notificationRepository);
        this.notificationRepository = notificationRepository;
        this.emailSenderService = emailSenderService;
    }

    @Override
    public void create(Notification notification, Transaction transaction) {
        notificationRepository.create(notification);
        emailSenderService.sendEmailNotification(notification, transaction);
    }


    @Override
    public void update(Notification notification) {
        notificationRepository.update(notification);
    }

    @Override
    public List<Notification> getAllForUser(int userId) {
        return notificationRepository.getAllForUser(userId);
    }

    @Override
    public Long getUnread(int userId) {
        return notificationRepository.getUnread(userId);
    }

    @Override
    public void deleteAllUserNotifications(int userId) {
        notificationRepository.deleteAllUserNotifications(userId);
    }

    @Override
    public void markAsRead(Notification notification, int userId) {
        if (isNotAuthorized(notification, userId)) {
            throw new UnauthorizedOperationException(String.format(INVALID_ROLE_ERROR_MESSAGE, "read"));
        }
        notification.setRead(true);
        notificationRepository.update(notification);
    }

    @Override
    public void delete(Notification notification, int userId) {
        if (isNotAuthorized(notification, userId)) {
            throw new UnauthorizedOperationException(String.format(INVALID_ROLE_ERROR_MESSAGE, "delete"));
        }
        notificationRepository.delete(notification);
    }

    private boolean isNotAuthorized(Notification notification, int userId) {
        return notification.getUser().getId() != userId;
    }
}
