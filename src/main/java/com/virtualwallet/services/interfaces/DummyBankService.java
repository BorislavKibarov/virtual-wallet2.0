package com.virtualwallet.services.interfaces;

import org.springframework.http.HttpStatus;

public interface DummyBankService {
    HttpStatus getDummyBankResponse();
}
