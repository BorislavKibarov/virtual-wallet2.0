package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService extends BaseGetService<User>, CreatableService<User>, UpdatableService<User>, DeletableService<User> {

    User getByUsername(String username);

    User getByAllFields(String value);

    List<User> search(Optional<String> keyWord);

    List<User> getRecipients(int userId);

    void upload(MultipartFile file, User user) throws IOException;

    void confirmUser(String uuid);
}
