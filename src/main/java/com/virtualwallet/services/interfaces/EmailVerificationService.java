package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.EmailVerification;
import com.virtualwallet.models.User;

public interface EmailVerificationService extends BaseGetService<EmailVerification> {
    EmailVerification create(User user);
}
