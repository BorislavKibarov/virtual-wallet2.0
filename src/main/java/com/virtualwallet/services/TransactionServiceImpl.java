package com.virtualwallet.services;

import com.virtualwallet.exceptions.DummyBankServerException;
import com.virtualwallet.exceptions.InsufficientFundsException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.*;
import com.virtualwallet.repositories.interfaces.TransactionRepository;
import com.virtualwallet.services.interfaces.DummyBankService;
import com.virtualwallet.services.interfaces.NotificationService;
import com.virtualwallet.services.interfaces.TransactionService;
import com.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl extends BaseGetServiceImpl<Transaction> implements TransactionService {

    private final static String INVALID_ROLE_ERROR_MESSAGE = "You can only view your own transactions.";
    private static final int DUMMY_BANK_USER_ID = 1;
    public static final String BLOCKED_USER_ERROR_MESSAGE = "You do not have permission to make transactions.";
    public static final String EMAIL_NOT_CONFIRMED_ERROR_MESSAGE = "You must first confirm your registration in order to make transactions. Please check your email for the confirmation link.";
    private static final String SELF_TRANSFER_ERROR_MESSAGE = "You can't make transfers to yourself";

    private final TransactionRepository repository;
    private final WalletService walletService;
    private final DummyBankService dummyBankService;
    private final NotificationService notificationService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository repository, WalletService walletService, DummyBankService dummyBankService, NotificationService notificationService) {
        super(repository);
        this.repository = repository;
        this.walletService = walletService;
        this.dummyBankService = dummyBankService;
        this.notificationService = notificationService;
    }

    @Override
    public List<Transaction> getAllForUser(User authenticatedUser,
                                           int id,
                                           Optional<LocalDate> fromDate,
                                           Optional<LocalDate> toDate,
                                           Optional<Integer> senderId,
                                           Optional<Integer> recipientId,
                                           Optional<String> sortAmount,
                                           Optional<String> sortDate) {
        if (authenticatedUser.getId() != id && authenticatedUser.getRole().equals(Role.valueOf("USER"))) {
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR_MESSAGE);
        }
        return repository.getAllForUser(id, fromDate, toDate, senderId, recipientId, sortAmount, sortDate);
    }

    @Override
    public List<Transaction> getLastThree(int userId) {
        return repository.getLastThree(userId);
    }

    @Override
    public void create(Transaction transaction) {
        if (transaction.getSender().getId() == transaction.getRecipient().getId()) {
            throw new UnauthorizedOperationException(SELF_TRANSFER_ERROR_MESSAGE);
        }
        if (!transaction.getSender().isEmailConfirmed()) {
            throw new UnauthorizedOperationException(EMAIL_NOT_CONFIRMED_ERROR_MESSAGE);
        }
        if (transaction.getSender().getId() != DUMMY_BANK_USER_ID) {
            if (transaction.getSender().getStatus().equals(Status.BLOCKED)) {
                throw new UnauthorizedOperationException(BLOCKED_USER_ERROR_MESSAGE);
            }
            walletService.doubleUpdate(transaction);
            notificationService.create(buildNotification(transaction), transaction);
        } else {
            HttpStatus dummyBankResponse = dummyBankService.getDummyBankResponse();
            if (dummyBankResponse.is4xxClientError()) {
                throw new InsufficientFundsException();
            }
            if (dummyBankResponse.is5xxServerError()) {
                throw new DummyBankServerException();
            }
            walletService.singleUpdate(transaction);
        }
        repository.create(transaction);
    }

    private Notification buildNotification(Transaction transaction) {
        Notification notification = new Notification();
        notification.setUser(transaction.getRecipient());
        notification.setMessage(String.format("You have received %s BGN from %s.", transaction.getAmount(), transaction.getSender().getUsername()));
        notification.setCreationDate(LocalDate.now());

        return notification;
    }
}
