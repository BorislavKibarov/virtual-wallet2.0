package com.virtualwallet.mappers;

import com.virtualwallet.models.Role;
import com.virtualwallet.models.State;
import com.virtualwallet.models.Status;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.PasswordDto;
import com.virtualwallet.models.dtos.RegisterDto;
import com.virtualwallet.models.dtos.UpdateUserDto;
import com.virtualwallet.models.dtos.UserDto;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private static final String DEFAULT_AVATAR_URL = "https://res.cloudinary.com/dm9jsxeij/image/upload/v1633438332/nk1rzh2vnkqynagdziml.jpg";
    private final UserService userService;

    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User fromUserDtoToUserObject(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        user.setStatus(Status.UNBLOCKED);
        user.setRole(Role.USER);
        user.setState(State.ACTIVATED);
        user.setAvatarUrl(DEFAULT_AVATAR_URL);
        return user;
    }

    public User fromUpdateUserDtoToUserObject(UpdateUserDto updateUserDto, User user) {
        dtoToObject(updateUserDto, user);
        return user;
    }

    public UpdateUserDto fromUserToUpdateUserDto(User user) {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());

        return userDto;
    }

    public User changeUserPassword(PasswordDto passwordDto, User user) {
        user.setPassword(passwordDto.getNewPassword());

        return user;
    }

    public User changeUserStatus(int id) {
        User user = userService.getById(id);
        if (user.getStatus().equals(Status.BLOCKED)) {
            user.setStatus(Status.UNBLOCKED);
        } else {
            user.setStatus(Status.BLOCKED);
        }
        return user;
    }

    public User changeUserRole(int id) {
        User user = userService.getById(id);
        user.setRole(Role.ADMIN);
        return user;
    }

    public UserDto fromRegisterDtoToUserDto(RegisterDto registerDto) {
        UserDto userDto = new UserDto();
        userDto.setUsername(registerDto.getUsername());
        userDto.setPassword(registerDto.getPassword());
        userDto.setEmail(registerDto.getEmail());
        userDto.setPhoneNumber(registerDto.getPhoneNumber());

        return userDto;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
    }

    private void dtoToObject(UpdateUserDto updateUserDto, User user) {
        user.setEmail(updateUserDto.getEmail());
        user.setPhoneNumber(updateUserDto.getPhoneNumber());
    }
}
