package com.virtualwallet.models;

public enum Direction {
    INCOMING("Incoming"), OUTGOING("Outgoing");

    private final String displayValue;

    Direction(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
