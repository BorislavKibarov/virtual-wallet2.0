package com.virtualwallet.models;

public enum Role {
    USER("User"), ADMIN("Admin");

    private final String displayValue;

    Role(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
