package com.virtualwallet.models;

public enum State {
    ACTIVATED("Activated"), DEACTIVATED("Deactivated");

    private final String displayValue;

    State(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
