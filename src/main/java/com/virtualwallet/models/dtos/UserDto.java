package com.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class UserDto {
    public static final String USERNAME_INVALID = "Username cannot be empty.";
    public static final String USERNAME_LENGTH_INVALID = "Username should be between 2 and 20 symbols.";
    public static final String PASSWORD_INVALID = "Password cannot be empty.";
    public static final String PASSWORD_LENGTH_INVALID = "Password should be at least 2 symbols.";
    public static final String PASSWORD_PATTERN_INVALID = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)";
    public static final String PHONE_NUMBER_INVALID = "Phone number cannot be empty.";
    public static final String PHONE_NUMBER_LENGTH_INVALID = "Phone number should be at least 10 digits.";
    public static final String PHONE_NUMBER_PATTERN_INVALID = "Phone number must be 10 digits.";

    @NotNull(message = USERNAME_INVALID)
    @Size(min = 2, max = 20, message = USERNAME_LENGTH_INVALID)
    private String username;

    @NotNull(message = PASSWORD_INVALID)
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$", message = PASSWORD_PATTERN_INVALID)
    private String password;

    @Email

    private String email;

    @Pattern(regexp = "^[0-9]{10}$", message = PHONE_NUMBER_PATTERN_INVALID)
    @NotNull(message = PHONE_NUMBER_INVALID)
    private String phoneNumber;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
