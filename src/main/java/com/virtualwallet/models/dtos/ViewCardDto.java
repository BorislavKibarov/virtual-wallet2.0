package com.virtualwallet.models.dtos;

import com.virtualwallet.models.Card;
import com.virtualwallet.models.CardStatus;
import com.virtualwallet.models.Type;

import java.time.LocalDate;

public class ViewCardDto {

    private int id;

    private String cardNumber;

    private String holder;

    private Type type;

    private CardStatus cardStatus;

    public ViewCardDto(Card card) {
        setId(card.getId());
        setCardNumber(card.getCardNumber());
        setHolder(card.getCardHolder());
        setType(card.getType());
        if (card.getExpirationDate().isBefore(LocalDate.now())) {
            setCardStatus(CardStatus.EXPIRED);
        } else {
            setCardStatus(CardStatus.ACTIVE);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = "**** **** **** " + cardNumber.substring(12,16);
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }
}
