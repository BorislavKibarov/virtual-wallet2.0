package com.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class RegisterDto extends UserDto {

    public static final String PASSWORD_CONFIRMATION_INVALID = "Password confirmation must match Password";

    @NotEmpty(message = PASSWORD_CONFIRMATION_INVALID)
    private String passwordConfirm;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
