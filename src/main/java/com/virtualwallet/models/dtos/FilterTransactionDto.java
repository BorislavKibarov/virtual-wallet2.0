package com.virtualwallet.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

import static com.virtualwallet.controllers.rest.TransactionRestController.DATE_TIME_FORMAT_PATTERN;

public class FilterTransactionDto {

    @DateTimeFormat(pattern = DATE_TIME_FORMAT_PATTERN)
    private LocalDate fromDate;

    @DateTimeFormat(pattern = DATE_TIME_FORMAT_PATTERN)
    private LocalDate toDate;

    private Integer senderId;

    private Integer recipientId;

    public FilterTransactionDto() {
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }
}
