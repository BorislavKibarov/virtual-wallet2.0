package com.virtualwallet.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.virtualwallet.models.dtos.UserDto.*;

public class UpdateUserDto {

    @Email
    private String email;

    @Pattern(regexp = "^[0-9]{10}$", message = PHONE_NUMBER_PATTERN_INVALID)
    @NotNull(message = PHONE_NUMBER_INVALID)
    @Size(min = 2, message = PHONE_NUMBER_LENGTH_INVALID)
    private String phoneNumber;

    public UpdateUserDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
