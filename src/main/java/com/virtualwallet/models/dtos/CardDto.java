package com.virtualwallet.models.dtos;

import com.virtualwallet.models.Type;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CardDto {
    private static final String CARD_NUMBER_LENGTH_INVALID = "Card number should be exactly 16 digits.";
    private static final String CARD_HOLDER_LENGTH_INVALID = "Card holder should be between 2 and 30 symbols.";
    private static final String CHECK_NUMBER_LENGTH_INVALID = "Check number should be exactly 3 digits.";
    private static final String EXPIRATION_DATE_FORMAT_ERROR_MESSAGE = "Please enter the expiration date in [MM/YY] format!";

    @Pattern(regexp = "([0-9]{16})", message = CARD_NUMBER_LENGTH_INVALID)
    private String cardNumber;

    @Size(min = 2, max = 30, message = CARD_HOLDER_LENGTH_INVALID)
    private String cardHolder;

    @Pattern(regexp = "(0[1-9]|1[0-2])/([0-9]{2})", message = EXPIRATION_DATE_FORMAT_ERROR_MESSAGE)
    private String expirationDate;

    @Pattern(regexp = "([0-9]{3})", message = CHECK_NUMBER_LENGTH_INVALID)
    private String checkNumber;

    @Enumerated(EnumType.STRING)
    private Type type;

    public CardDto() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
