package com.virtualwallet.models;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "email_verifications")
public class EmailVerification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id")
    private int id;

    @Column(name = "token_value")
    private String tokenValue;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public EmailVerification(User user) {
        this.user = user;
        this.tokenValue = UUID.randomUUID().toString();
    }

    public EmailVerification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
