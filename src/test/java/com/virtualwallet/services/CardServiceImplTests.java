package com.virtualwallet.services;

import com.virtualwallet.models.Card;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.CardRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CardServiceImplTests {

    @Mock
    CardRepository mockCardRepository;

    @InjectMocks
    CardServiceImpl mockCardService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockCardRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCardService.getAll();

        Mockito.verify(mockCardRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCard_when_matchExist() {
        Card mockCard = createMockCard();
        Mockito.when(mockCardRepository.getById(mockCard.getId()))
                .thenReturn(mockCard);

        Card result = mockCardService.getById(mockCard.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCard.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCard.getCardHolder(), result.getCardHolder()),
                () -> Assertions.assertEquals(mockCard.getCardNumber(), result.getCardNumber()),
                () -> Assertions.assertEquals(mockCard.getCheckNumber(), result.getCheckNumber()),
                () -> Assertions.assertEquals(mockCard.getUser().getId(), result.getUser().getId())
        );
    }

    @Test
    void getAllForUser_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockCardRepository.getAllForUser(mockUser.getId())).thenReturn(new ArrayList<>());

        mockCardService.getAllForUser(mockUser.getId());

        Mockito.verify(mockCardRepository, Mockito.times(1)).getAllForUser(mockUser.getId());
    }

    @Test
    void create_should_callRepository() {
        Card mockCard = createMockCard();

        mockCardService.create(mockCard);

        Mockito.verify(mockCardRepository, Mockito.times(1)).create(mockCard);
    }

    @Test
    void delete_should_callRepository() {
        Card mockCard = createMockCard();

        mockCardService.delete(mockCard, mockCard.getUser().getId());

        Mockito.verify(mockCardRepository, Mockito.times(1)).delete(mockCard);
    }

    @Test
    void deleteAllForUser_should_callRepository() {
        mockCardService.deleteAllForUser(Mockito.anyInt());

        Mockito.verify(mockCardRepository, Mockito.times(1)).deleteAllForUser(Mockito.anyInt());
    }
}
