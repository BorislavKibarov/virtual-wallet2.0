package com.virtualwallet.services;

import com.virtualwallet.exceptions.DummyBankServerException;
import com.virtualwallet.exceptions.InsufficientFundsException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static com.virtualwallet.Helpers.*;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTests {

    @Mock
    TransactionRepository mockTransactionRepository;

    @Mock
    WalletServiceImpl mockWalletService;

    @Mock
    DummyBankServiceImpl mockDummyBankService;

    @Mock
    NotificationServiceImpl mockNotificationService;

    @InjectMocks
    TransactionServiceImpl mockTransactionService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockTransactionRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockTransactionService.getAll();

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnTransaction_when_matchExist() {
        Transaction mockTransaction = createMockTransaction();

        Mockito.when(mockTransactionRepository.getById(mockTransaction.getId()))
                .thenReturn(mockTransaction);

        Transaction result = mockTransactionService.getById(mockTransaction.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTransaction.getId(), result.getId()),
                () -> Assertions.assertEquals(mockTransaction.getSender(), result.getSender()),
                () -> Assertions.assertEquals(mockTransaction.getRecipient(), result.getRecipient()),
                () -> Assertions.assertEquals(mockTransaction.getDate(), result.getDate()),
                () -> Assertions.assertEquals(mockTransaction.getAmount(), result.getAmount())
        );
    }

    @Test
    void getAllForUser_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockTransactionRepository.getAllForUser(
                        mockUser.getId(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        mockTransactionService.getAllForUser(
                mockUser, mockUser.getId(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getAllForUser(
                        mockUser.getId(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    void getAllForUser_should_throw_when_user_isNotOwnerOrAdmin() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);

       Assertions.assertThrows(UnauthorizedOperationException.class, () -> mockTransactionService.getAllForUser(
               mockUser2, mockUser.getId(), Optional.empty(), Optional.empty(),
               Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));
    }

    @Test
    public void create_should_callRepository() {
        Transaction mockTransaction = createMockTransaction();
        User mockRecipient = createMockUser();
        mockRecipient.setId(2);
        mockTransaction.setRecipient(mockRecipient);

        mockTransactionService.create(mockTransaction);

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .create(mockTransaction);
    }

    @Test
    public void create_should_callWalletService_when_between_users() {
        Transaction mockTransaction = createMockTransaction();
        User mockRecipient = createMockUser();
        mockRecipient.setId(2);
        mockTransaction.setRecipient(mockRecipient);

        mockTransactionService.create(mockTransaction);

        Mockito.verify(mockWalletService, Mockito.times(1))
                .doubleUpdate(mockTransaction);
    }

    @Test
    public void create_should_callWalletService_when_deposit() {
        Transaction mockTransaction = createMockTransaction();
        User mockDummyBank = createMockUser();
        mockDummyBank.setId(1);
        mockTransaction.setSender(mockDummyBank);

        Mockito.when(mockDummyBankService.getDummyBankResponse()).thenReturn(HttpStatus.OK);
        mockTransactionService.create(mockTransaction);

        Mockito.verify(mockWalletService, Mockito.times(1))
                .singleUpdate(mockTransaction);
    }

    @Test
    public void create_should_throw_when_bankResponseIs_badRequest() {
        Transaction mockTransaction = createMockTransaction();
        User mockDummyBank = createMockUser();
        mockDummyBank.setId(1);
        mockTransaction.setSender(mockDummyBank);

        Mockito.when(mockDummyBankService.getDummyBankResponse()).thenReturn(HttpStatus.BAD_REQUEST);

        Assertions.assertThrows(InsufficientFundsException.class, () -> mockTransactionService.create(mockTransaction));
    }

    @Test
    public void create_should_throw_when_bankResponseIs_badGateway() {
        Transaction mockTransaction = createMockTransaction();
        User mockDummyBank = createMockUser();
        mockDummyBank.setId(1);
        mockTransaction.setSender(mockDummyBank);

        Mockito.when(mockDummyBankService.getDummyBankResponse()).thenReturn(HttpStatus.BAD_GATEWAY);

        Assertions.assertThrows(DummyBankServerException.class, () -> mockTransactionService.create(mockTransaction));
    }
}
