package com.virtualwallet.services;

import com.virtualwallet.controllers.CloudinaryHelper;
import com.virtualwallet.exceptions.DuplicateEntityException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.UserRepository;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.EmailSenderService;
import com.virtualwallet.services.interfaces.WalletService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static com.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    EmailSenderService mockEmailSenderService;

    @Mock
    WalletService mockWalletService;

    @Mock
    CardService mockCardService;

    @Mock
    CloudinaryHelper cloudinaryHelper;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockUserService.getAll();

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = mockUserService.getById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getState(), result.getState()),
                () -> Assertions.assertEquals(mockUser.getStatus(), result.getStatus())
        );
    }

    @Test
    void getByUsername_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(new User());

        mockUserService.getByUsername(mockUser.getUsername());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getByField(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    void getByAllFields_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByAllFields(Mockito.anyString()))
                .thenReturn(new User());

        mockUserService.getByAllFields(mockUser.getUsername());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getByAllFields(Mockito.anyString());
    }

    @Test
    void search_should_callRepository() {
        Optional<String> mockKeyword = Optional.of("mockKeyWord");
        Mockito.when(mockUserRepository.search(mockKeyword))
                .thenReturn(new ArrayList<>());

        mockUserService.search(mockKeyword);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .search(mockKeyword);
    }

    @Test
    void upload_should_callRepository() throws IOException {
        User mockUser = createMockUser();
        MultipartFile mockFile = new MockMultipartFile("foo", "foo.txt", MediaType.TEXT_PLAIN_VALUE,
                "Hello World".getBytes());
        Mockito.when(cloudinaryHelper.upload(mockFile)).thenReturn("url");

        mockUserService.upload(mockFile, mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void create_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        Mockito.doNothing().when(mockEmailSenderService).sendEmailConfirmation(mockUser);
        mockUserService.create(mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(1)).create(mockUser);
    }

    @Test
    void create_should_callWalletService() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUser);
        Mockito.doNothing().when(mockEmailSenderService).sendEmailConfirmation(mockUser);
        mockUserService.create(mockUser);

        Mockito.verify(mockWalletService, Mockito.times(1)).create(Mockito.any());
    }

    @Test
    void create_should_throw_when_usernameIsAlreadyTaken() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserService.create(mockUser2));
    }

    @Test
    void create_should_throw_when_emailIsAlreadyTaken() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setUsername("MockUsername2");

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class)
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserService.create(mockUser2));
    }

    @Test
    void create_should_throw_when_phoneNumberIsAlreadyTaken() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setUsername("MockUsername2");
        mockUser2.setEmail("mock2@user.com");

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class)
                .thenThrow(EntityNotFoundException.class)
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserService.create(mockUser2));
    }

    @Test
    void update_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString())).thenReturn(mockUser);
        mockUserService.update(mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void update_should_throw_when_emailIsAlreadyTaken() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserService.update(mockUser2));
    }

    @Test
    void update_should_throw_when_phoneNumberIsAlreadyTaken() {
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setEmail("mock2@user.com");

        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mockUser2, mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserService.update(mockUser2));
    }

    @Test
    void delete_should_callRepository() {
        User mockUser = createMockUser();

        mockUserService.delete(mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    void delete_should_callWalletService() {
        User mockUser = createMockUser();

        mockUserService.delete(mockUser);

        Mockito.verify(mockWalletService, Mockito.times(1)).delete(Mockito.any());
    }

    @Test
    void delete_should_callCardService() {
        User mockUser = createMockUser();

        mockUserService.delete(mockUser);

        Mockito.verify(mockCardService, Mockito.times(1)).deleteAllForUser(Mockito.anyInt());
    }
}
