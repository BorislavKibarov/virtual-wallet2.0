package com.virtualwallet;

import com.virtualwallet.models.*;

import java.math.BigDecimal;
import java.time.LocalDate;


public class Helpers {

    public static User createMockUser() {
        return createMockUser(Role.USER);
    }

    public static User createMockAdmin() {
        return createMockUser(Role.ADMIN);
    }

    private static User createMockUser(Role role) {
        var mockUser = new User();
        mockUser.setId(3);
        mockUser.setUsername("MockUsername");
        mockUser.setEmail("mock@user.com");
        mockUser.setPassword("MockPassword");
        mockUser.setPhoneNumber("0888567891");
        mockUser.setEmailConfirmed(true);
        mockUser.setRole(role);
        mockUser.setState(State.ACTIVATED);
        mockUser.setStatus(Status.UNBLOCKED);
        return mockUser;
    }

    public static Transaction createMockTransaction() {
        var mockTransaction = new Transaction();
        mockTransaction.setId(1);
        mockTransaction.setSender(createMockUser());
        mockTransaction.setRecipient(createMockUser());
        mockTransaction.setDate(LocalDate.now());
        mockTransaction.setAmount(BigDecimal.valueOf(1000));
        return mockTransaction;
    }

    public static Card createMockCard() {
        var mockCard = new Card();
        mockCard.setId(1);
        mockCard.setCardNumber("MockCardNumber");
        mockCard.setCardHolder("MockCardHolder");
        mockCard.setUser(createMockUser());
        mockCard.setExpirationDate(LocalDate.now().plusDays(10));
        mockCard.setType(Type.DEBIT);
        return mockCard;
    }

    public static Wallet createMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setId(1);
        mockWallet.setUser(createMockUser());
        mockWallet.setBalance(BigDecimal.valueOf(1000));
        return mockWallet;
    }

    public static Notification createMockNotification() {
        var mockNotification = new Notification();
        mockNotification.setId(1);
        mockNotification.setUser(createMockUser());
        mockNotification.setMessage("message");
        mockNotification.setCreationDate(LocalDate.now());
        return mockNotification;
    }
}
