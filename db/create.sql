-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for vwallet
CREATE
DATABASE IF NOT EXISTS `vwallet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE
`vwallet`;

-- Dumping structure for table vwallet.cards
CREATE TABLE IF NOT EXISTS `cards`
(
    `card_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `card_holder` varchar
(
    30
) NOT NULL,
    `card_number` varchar
(
    16
) NOT NULL,
    `expiration_date` datetime DEFAULT NULL,
    `check_number` varchar
(
    3
) NOT NULL,
    `user_id` int
(
    11
) NOT NULL,
    `type` enum
(
    'CREDIT',
    'DEBIT'
) NOT NULL,
    PRIMARY KEY
(
    `card_id`
),
    KEY `cards_users_user_id_fk`
(
    `user_id`
),
    CONSTRAINT `cards_users_user_id_fk` FOREIGN KEY
(
    `user_id`
) REFERENCES `users`
(
    `user_id`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table vwallet.email_verifications
CREATE TABLE IF NOT EXISTS `email_verifications`
(
    `token_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `token_value` varchar
(
    500
) NOT NULL,
    `user_id` int
(
    11
) NOT NULL,
    PRIMARY KEY
(
    `token_id`
),
    KEY `email_verifications_users_user_id_fk`
(
    `user_id`
),
    CONSTRAINT `email_verifications_users_user_id_fk` FOREIGN KEY
(
    `user_id`
) REFERENCES `users`
(
    `user_id`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table vwallet.notifications
CREATE TABLE IF NOT EXISTS `notifications`
(
    `notification_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `message` varchar
(
    500
) NOT NULL,
    `user_id` int
(
    11
) NOT NULL,
    `is_read` tinyint
(
    1
) NOT NULL,
    `creation_date` date NOT NULL,
    PRIMARY KEY
(
    `notification_id`
),
    UNIQUE KEY `notifications_notification_id_uindex`
(
    `notification_id`
),
    KEY `notifications_users_user_id_fk`
(
    `user_id`
),
    CONSTRAINT `notifications_users_user_id_fk` FOREIGN KEY
(
    `user_id`
) REFERENCES `users`
(
    `user_id`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table vwallet.transactions
CREATE TABLE IF NOT EXISTS `transactions`
(
    `transaction_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `sender_id` int
(
    11
) NOT NULL,
    `recipient_id` int
(
    11
) NOT NULL,
    `date` date DEFAULT NULL,
    `amount` decimal
(
    19,
    2
) NOT NULL,
    PRIMARY KEY
(
    `transaction_id`
),
    KEY `transactions_users_user_id_fk`
(
    `sender_id`
),
    KEY `transactions_users_user_id_fk_2`
(
    `recipient_id`
),
    CONSTRAINT `transactions_users_user_id_fk` FOREIGN KEY
(
    `sender_id`
) REFERENCES `users`
(
    `user_id`
),
    CONSTRAINT `transactions_users_user_id_fk_2` FOREIGN KEY
(
    `recipient_id`
) REFERENCES `users`
(
    `user_id`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table vwallet.users
CREATE TABLE IF NOT EXISTS `users`
(
    `user_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `username` varchar
(
    20
) NOT NULL,
    `password` varchar
(
    20
) NOT NULL,
    `email` varchar
(
    50
) NOT NULL,
    `phone_number` varchar
(
    10
) NOT NULL,
    `status` enum
(
    'BLOCKED',
    'UNBLOCKED'
) NOT NULL,
    `role` enum
(
    'USER',
    'ADMIN'
) NOT NULL,
    `state` enum
(
    'ACTIVATED',
    'DEACTIVATED'
) NOT NULL,
    `image_url` varchar
(
    500
) NOT NULL,
    `email_confirmed` tinyint
(
    1
) DEFAULT 0,
    PRIMARY KEY
(
    `user_id`
),
    UNIQUE KEY `users_email_uindex`
(
    `email`
),
    UNIQUE KEY `users_username_uindex`
(
    `username`
),
    UNIQUE KEY `users_phone_number_uindex`
(
    `phone_number`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table vwallet.wallets
CREATE TABLE IF NOT EXISTS `wallets`
(
    `wallet_id` int
(
    11
) NOT NULL AUTO_INCREMENT,
    `balance` decimal
(
    10,
    0
) NOT NULL,
    `user_id` int
(
    11
) DEFAULT NULL,
    PRIMARY KEY
(
    `wallet_id`
),
    KEY `wallets_users_user_id_fk`
(
    `user_id`
),
    CONSTRAINT `wallets_users_user_id_fk` FOREIGN KEY
(
    `user_id`
) REFERENCES `users`
(
    `user_id`
)
    ) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
